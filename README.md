ansible-role-deploy-users
=========
[![pipeline status](https://gitlab.com/offtechnologies/ansible-role-demo-user/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/ansible-role-demo-user/commits/master)

[offtechurl]: https://gitlab.com/offtechnologies
[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]

Manages deploy user on Debian systems.

Requirements
------------
None

Role Variables
--------------

see `defaults/main.yml` and `vars/main.yml`

Dependencies
------------

None

Example Playbook
----------------

```yaml
---

- name: Configure dev system(s)
  hosts: all
  roles:
    - { role: ansible-role-demo-user }
```

License
-------

BSD
