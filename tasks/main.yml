---
# tasks file for ansible-role-demo-user

- name: Change root password
  become: true
  user:
    name: "root"
    update_password: always
    # An idempotent method to generate unique hashes per
    # system is to use a salt that is consistent between runs:
    # https://docs.ansible.com/ansible/2.5/user_guide/playbooks_filters.html
    password: "{{ root_password |  password_hash('sha512', 65534 | random(seed=inventory_hostname) | string) }}"
  no_log: true

- name: Set admin_groups
  set_fact: admin_groups={{ [global_admin_groups] }}

- name: Create admin groups
  group: name={{ item }} state=present
  with_items: "{{ admin_groups }}"

- name: Define admin users
  set_fact:
    admin_users: "{{ global_admin_users | unique }}"

- name: Define authorized users
  set_fact:
    all_users: "{{ global_admin_users | union(regular_users) | unique }}"

- name: Allow users in admin group to run sudo
  template: src=00-admin dest=/etc/sudoers.d/00-admin owner=root group=root mode=0440
  when: admin_users | length > 0

# Ansible-lint - Rule 306 -  Shells that use pipes should set the pipefail option
- name: Retrieve existing users
  shell: "set -o pipefail && cat /etc/passwd | awk -F ':' '$5 ~ /^ansible-/' | cut -d ':' -f 1"
  register: existing_users
  check_mode: false
  changed_when: false
  args:
    executable: /bin/bash

- name: Define deleted users
  set_fact:
    deleted_users: "{{ existing_users.stdout_lines | difference(all_users) }}"

- name: Remove deleted users
  user:
    name: "{{ item }}"
    state: absent
    remove: "{{ item.remove | default(delete_remove) }}"
    force: "{{ item.force | default(delete_force) }}"
  with_items: "{{ deleted_users }}"

- name: Remove deleted legacy user accounts
  user:
    name: "{{ item.user }}"
    state: absent
    remove: "{{ item.remove | default(delete_remove) }}"
    force: "{{ item.force | default(delete_force) }}"
  with_items: "{{ delete_users }}"

# http://docs.ansible.com/ansible/user_module.html
- name: Add admin users
  user:
    name: "{{ item }}"
    groups: "{{ admin_groups | join(',') }}"
    comment: "ansible-{{ item }}"
    shell: /bin/bash
    append: false
  with_items: "{{ global_admin_users | union(admin_users) }}"

# http://docs.ansible.com/ansible/authorized_key_module.html
- name: Set ssh keys for admin users
  authorized_key:
    user: "{{ item.user }}"
    key: "{{ item.key }}"
    exclusive: true
  with_items: "{{ users }}"
  when: "item.key is defined and item.user in admin_users"
  no_log: true

- name: Create regular groups
  group: name={{ item }} state=present
  with_items: "{{ regular_groups }}"

- name: Add regular users
  user:
    name: "{{ item }}"
    groups: "{{ regular_groups | join(',') }}"
    comment: "ansible-{{ item }}"
    shell: /bin/bash
  with_items: "{{ regular_users }}"

- name: Set ssh keys for regular users from files
  authorized_key:
    user: "{{ item.user }}"
    key: "{{ item.key }}"
    exclusive: true
  with_items: "{{ users }}"
  when: item.key is defined and item.user in regular_users
  no_log: true

- name: Remove authorized_keys for system accounts
  file:
    path: "{{ item }}/.ssh/authorized_keys"
    state: absent
  with_items:
    - /home/debian
  when: remove_system_authorized_keys | bool
